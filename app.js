const express = require('express')
const AppConfig = require('./config')
const app = express()

//指定静态资源文件夹
// appConfig(app)   //这是函数的封装写法
let appConfig  = new AppConfig(app) //这是以面向对象的方式出现
//先要设置cookie session


app.listen(appConfig.listenPort, () => {
    console.log(`服务器已经启动，端口为: ${appConfig.listenPort}`);
})