//一些配置信息

const path = require("path");
const express = require("express");
const cookieParser = require("cookie-parser");
const cookieSession = require("cookie-session");
const indexRouter = require('./routes/index.js')
const passportRouter = require('./routes/passport.js')
const detailRouter = require('./routes/detail.js')
const common = require('./utils/common')
const encodeRouter = require('./routes/test')
const keys = require('./key')

//封装成一个函数
// let appConfig = app => {
//     app.use(express.static("public"));

//     //1.模板的配置
//     // 1、修改模板引擎为html，导入express-art-template
//     app.engine("html", require("express-art-template"));
//     // 2、设置运行的模式为开发模式
//     app.set("view options", {
//       debug: process.env.NODE_ENV !== "development",
//     });
//     // 3、设置模板存放目录为views文件夹
//     app.set("views", path.join(__dirname, "views"));
//     // 4、设置引擎后缀为html
//     app.set("view engine", "html");

//     //注册cookie和session
//     app.use(cookieParser());
//     app.use(
//       cookieSession({
//         name: "my_session",
//         keys: ["$%%&%&$%^&*&^%$%^"],
//         maxAge: 1000 * 60 * 60 * 24 * 2, //两天
//       })
//     );

//     //2.获取post请求的配置
//     app.use(express.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded   针对普通页面提交功能
//     app.use(express.json()); // parse application/json    针对异步提交ajax
// }

//这是以面向对象的写法来封装我们的配置
class AppConfig {
  constructor(app) {
    this.app = app;
    this.listenPort = 3000;
    this.app.use(express.static("public"));

    //1.模板的配置
    // 1、修改模板引擎为html，导入express-art-template
    this.app.engine("html", require("express-art-template"));
    // 2、设置运行的模式为开发模式
    this.app.set("view options", {
      debug: process.env.NODE_ENV !== "development",
    });
    // 3、设置模板存放目录为views文件夹
    this.app.set("views", path.join(__dirname, "views"));
    // 4、设置引擎后缀为html
    this.app.set("view engine", "html");

    //注册cookie和session
    this.app.use(cookieParser());
    this.app.use(
      cookieSession({
        name: "my_session",
        keys: [keys.session_key],
        maxAge: 1000 * 60 * 60 * 24 * 2, //两天
      })
    );
    
    //2.获取post请求的配置
    this.app.use(express.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded   针对普通页面提交功能
    this.app.use(express.json()); // parse application/json    针对异步提交ajax
    this.app.use(common.csrfProtect,indexRouter)
    this.app.use(common.csrfProtect,passportRouter)
    this.app.use(common.csrfProtect,detailRouter)
    // this.app.use(common.csrfProtect, encodeRouter)
    // this.app.use(indexRouter)
    // this.app.use(passportRouter)
    this.app.use((req, res) => {

     (async function() {
      let userInfo = await common.getUser(req, res);
      let data = {
        user_info: userInfo[0] ? {
            nick_name: userInfo[0].nick_name,
            avatar_url: userInfo[0].avatar_url
        } : false,
      }
      res.render('news/html/404', data)
     })();
    })
  }
}
module.exports = AppConfig;
