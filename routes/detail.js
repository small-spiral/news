const express = require("express");
const router = express.Router();
const handleDB = require("../db/handleDB");
const common = require("../utils/common");
require("../utils/filter.js");

router.get("/news_detail/:news_id", (req, res) => {
  (async function () {
    //访问首页 处理右上角是否登录展示问题
    //判断是否登录
    //从session中获取user_id
    let userInfo = await common.getUser(req, res);
    //右侧点击排行的查询
    let result3 = await handleDB(
      res,
      "info_news",
      "find",
      "数据库查询失败",
      "1 order by clicks desc limit 6"
    );
    let { news_id } = req.params;
    //左侧内容新闻的查询

    let newsResult = await handleDB(
      res,
      "info_news",
      "find",
      "数据库查询失败",
      `id=${news_id}`
    );
    //要确保有这篇新闻，如果没有这篇新闻的话我们就要返回404页面,要判断这篇新闻的id值
    if (!newsResult[0]) {
      let data = {
        user_info: userInfo[0]
          ? {
              nick_name: userInfo[0].nick_name,
              avatar_url: userInfo[0].avatar_url,
            }
          : false,
      };
      res.render("news/html/404", data);
      return;
    }
    newsResult[0].clicks += 1; //在我们访问一次这个新闻页面的时候，我们的点击量clicks就要加1
    await handleDB(
      res,
      "info_news",
      "update",
      "数据库添加失败",
      `id=${news_id}`,
      { clicks: newsResult[0].clicks }
    ); //更新数据库的clicks点击量，+1

    //判断用户是不是已经收藏了这篇文章，传一个布尔值给模板
    // 什么情况改变  用户登录了并且收藏了这一篇新闻
    // if(登录{
    //     //查询数据库
    //     iscollected  = true
    // })

    // 收餐功能
    // iscollected默认是false，也就是没有手残
    let isCollected = false;
    //判断用户有没有登录
    if (userInfo[0]) {
      //查询数据库，查询对应的id有没有对应的收藏文章
      let collectedResult = await handleDB(
        res,
        "info_user_collection",
        "find",
        "数据库查询失败",
        `user_id=${userInfo[0].id} and news_id=${news_id}`
      );
      //如果在数据库中查询到这一篇文章，就改变成为收藏状态
      if (collectedResult[0]) {
        isCollected = true;
      }
    }

    //判断是否从数据库查询的数据是否存在，如果存在的话，取出需要的参数，如果没有的话则返回false
    let data = {
      user_info: userInfo[0]
        ? {
            nick_name: userInfo[0].nick_name,
            avatar_url: userInfo[0].avatar_url,
          }
        : false,
      newsClick: result3,
      newsData: newsResult[0],
      isCollected,
    };
    res.render("news/html/detail", data);
  })();
});

router.post("/news_detail/news_collect", (req, res) => {
  //判断用户是否处于登录状态
  (async function () {
    let userInfo = await common.getUser(req, res);
    if (!userInfo[0]) {
      res.send({ errno: "4101", errmsg: "错误" });
      return;
    }
    //判断参数是否为空
    let { news_id, action } = req.body;
    if (!news_id || !action) {
      res.send({ errmsg: "错误参数不能为空" });
      return;
    }
    //查询数据库，看看参数是否存在
    let result = await handleDB(
      res,
      "info_news",
      "find",
      "查询数据库失败",
      `id=${news_id}`
    );
    if (!result[0]) {
      res.send({ errmsg: "没有在数据库中查询到数据" });
      return;
    }
    //根据action的值来判断是收藏还是取消收藏
    //返回操作的结果
    //执行收藏操作
    if (action === "collect") {
        //如果是action的话就收藏这篇文章到这个文章地下
      await handleDB(res, "info_user_collection", "insert", "数据库插入失败", {
        user_id: userInfo[0].id,
        news_id,
      });
    } else {
        //如果不是的话则就取消收餐
      await handleDB(
        res,
        "info_user_collection",
        "delete",
        "数据库删除失败",
        `user_id=${userInfo[0].id} and news_id=${news_id}`
      );
    }
    res.send({errno: "0", errmsg: '成功啦'})
  })();
});

//处理评论和回复的接口
router.post('/news_detail/news_comment', (req, res) => {
    //1.需要传哪些参数
    //一种是评论文章的  传入news_id  传入内容
    //一种是评论别人评论的评论   传入news_id 传入parents_id
    (async function() {
        //业务逻辑
        //1.获取登录信息，获取不到就return掉
        let userInfo = await common.getUser(req, res)
        if(!userInfo[0]) {
            res.send({errmsg: '错误'})
            return
        }
        //2.获取参数信息，判空
        
        let {news_id, comment, parent_id=null} = req.body   //获取参数
        if(!news_id || !comment ) {
            res.send({errmsg: '参数错误'})
            return
        }
        //3.查询新闻，看看这篇新闻是否存在
        let result =  await handleDB(res, "info_news", "find", "查询数据库失败", `id=${news_id}`)
        if(!result[0]) {
            res.send({errmsg: '参数错误2'})
            return
        }
        //4.往数据库info_comment中插入数据（如果有传parents_id，这个也是要穿的）
        let commentObj = {
            user_id: userInfo[0].id,
            news_id,
            content: comment,
            // create_time: new Date().toLocaleString()
        }
       if(parent_id) {  //如果传了parent_id  就设置了这个属性
           commentObj.parent_id = parent_id
       }
        let result2 = await handleDB(res, "info_comment", "insert", "数据库查询失败", commentObj);
        let obj = await handleDB(res, "info_comment", "find", "数据库查询失败", `user_id=${userInfo[0].id} and news_id=${news_id}`);
        let commentResult = await handleDB(res, "info_comment", "find", "数据库查询失败", `news_id=${news_id} order by create_time desc`)
        // console.log(commentResult);
        
        console.log(obj);
        let createTime = obj[0].create_time
        // console.log(result2);
        // 5.返回成功相应后的数据（传数据给前端）
        let data = {
            user: {
                avatar_url: userInfo[0].avatar_url ? userInfo[0].avatar_url : '/news/images/worm.jpg',
                nick_name: userInfo[0].nick_name,
            },
            content: comment,
            create_time: createTime,
            id: result2.insertId,
            news_id,
            commentList: commentResult

        }
        res.send({errno: "0", errmsg: "操作成功", data})
    })();
})

module.exports = router;
