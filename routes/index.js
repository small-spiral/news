const express = require("express");
const router = express.Router();
const handleDB = require("../db/handleDB");
const common = require("../utils/common")
require("../utils/filter");

router.get("/", (req, res) => {
  //设置cookie和session
  (async function () {
    //出力页面的右上角的问题
    //首先先判断一下页面有没有处于登录状态
    let userInfo = await common.getUser(req, res)
    // if(user_id) {
    //      result = await handleDB(res, 'info_user', 'find', '数据库查询失败', `id="${user_id}"`)
    // }
    //从数据库中获取到user_id的信息

    //--------------------------------------------------------------------------------------------
    // 上面头部菜单栏的信息显示
    let result2 = await handleDB(
      res,
      "info_category",
      "find",
      "数据库查询失败",
      ["name"]
    );

    //右侧菜单栏的信息显示
    // let result3 = await handleDB(res, 'info_news', 'sql', '数据库查询失败', 'select * from info_news order by clicks desc limit 6')
    let result3 = await handleDB(
      res,
      "info_news",
      "find",
      "数据库查询失败",
      "1 order by clicks desc limit 6"
    );

    let data = {
      user_info: userInfo[0]
        ? {
            avatar_url: userInfo[0].avatar_url,
            nick_name: userInfo[0].nick_name,
          }
        : false,
      category: result2,
      newsClick: result3,
    };
    return res.render("news/html/index", data);
  })();
});
router.get("/get_cookie", (req, res) => {
  //获取cookie
  res.send("获取到的cookie的值是" + req.cookies["name"]);
});
router.get("/get_session", (req, res) => {
  //获取session
  res.send("获取到的session的值是" + req.session["age"]);
});

//测试查询数据库
router.get("/get_data", (req, res) => {
  (async function () {
    let result = await handleDB(res, "info_category", "find", "数据库查询失败");
    res.send(result);
  })();
});

router.get("/news_list", (req, res) => {
  (async function () {
    //1.获取参数 cid(新闻分类) page(当前页面) perpage(页面的显示条数)
    let { cid = 1, page = 1, per_page=5 } = req.query; //为参数设置一个默认值
    let cont = cid == 1 ? '1' : `category_id=${cid}`
    //2.查询数据库  根据以上3个参数，获取前端需要的代码
    let result = await handleDB(res, "info_news", "limit", "数据出查询失败", {
      where: cont + ` order by create_time desc`,
      number: page,
      count: per_page,
    });

    //求总页数
    let result2= await handleDB(res, "info_news", "sql", "数据库查询失败", `select count(*) from info_news where ${cont}`  )
    console.log(result2);
    let totalPage = Math.ceil(result2[0]['count(*)'] / per_page);  //计算总共有多少页面  
    

    // 3.把查询到新闻数据的结构返回到客户短
    res.send({
      newsList: result,
      totalPage,
      currentPage: Number(page)

    });
  })();
});

module.exports = router;
