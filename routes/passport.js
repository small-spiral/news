//这是有关于验证的配置
const { Router } = require("express");
const express = require("express");
const handleDB = require("../db/handleDB");
const router = express.Router();
const Captcha = require("../utils/captcha/index");
const Moment = require("moment");
const md5 = require("md5")
const keys = require("../key")
const jwt = require("jsonwebtoken")

router.get("/passport/image_code/:float", (req, res) => {
  let captchaObj = new Captcha(); //创建了一个对象
  let captcha = captchaObj.getCode();

  //保存验证码到session当中去
  req.session["imageCode"] = captcha.text;
  console.log(req.session["imageCode"]);
  //配合img标签的src请求来展示验证码的时候，需要设置响应头
  res.setHeader("Content-Type", "image/svg+xml");
  res.send(captcha.data);
});

//1.判断用户填写的注册信息是否为空
router.post("/passport/register", (req, res) => {
  //使用立即执行函数
  (async function () {
    let { username, image_code, password, agree } = req.body;
    //判空
    if (!username || !image_code || !password || !agree) {
      res.send({ errmsg: "请输入参数信息" });
      return;
    }
    //3.判断用户输入的验证码正不正确
    //将用户输入的imageCode和我们session里面存的req.session["imageCode"]做对比
    if (image_code.toLowerCase() !== req.session["imageCode"].toLowerCase()) {
      res.send({ errmsg: "请填写正确的验证码" });
      return;
    }
    //查询数据库，看看用户名是否被注册
    let result = await handleDB(
      res,
      "info_user",
      "find",
      "查询用户信息失败",
      `username="${username}"`
    );
    //判断用户用户名已经被注册过了，则返回注册过的错误信息
    if (result[0]) {
      res.send({ errmsg: "用户名已经被注册过了" });
      return;
    }
    //如果没有被注册过，则在数据库中添加一条记录
    let result2 = await handleDB(
      res,
      "info_user",
      "insert",
      "添加用户信息失败",
      {
        username,
        nick_name: username,
        password_hash: md5(md5(password)+ keys.password_salt),
        // last_login: Moment(new Date().toLocaleString()).format('YYYY-MM-DD HH:mm:ss')
      }
    );
    //4.保持用户的登录状态

    req.session["user_id"] = result2.insertId; //插入数据的时候，自动生成的id
    //5.返回注册成功
    res.send({ errno: "0", errmsg: "注册成功" });
  })();
});

router.post("/passport/login", (req, res) => {
  (async function () {
    //1.判断用户名和密码是否为空，如果为空，则返回错误
    let { username, password } = req.body; //req.body是用来接收post请求的参数
    if (!username || !password) {
      res.send({ errmsg: "账号密码不能为空" });
      return;
    }
    //2.判断用户名是否正确，如果不正确，则返回错误
    let result = await handleDB(
      res,
      "info_user",
      "find",
      "数据库查询失败",
      `username="${username}"`
    );
    if (result.length === 0 || md5(md5(password)+keys.password_salt) !== result[0].password_hash) {
      res.send({ errmsg: "用户名或者密码填写错误" });
      return;
    }
    //3.判断用户的密码是否正确，如果不正确，则返回错误
    console.log(result);
    // if(password !== result[0].password_hash) {
    //   res.send({errmsg: '用户名或者密码填写错误'})
    //   return
    // }
    //添加最后登录时间的字段 last_login字段

    req.session["user_id"] = result[0].id; //登录状态的保持
    // await handleDB(
    //   res,
    //   "info_user",
    //   "update",
    //   "数据库更新失败",
    //   `id=${result[0].id}`,
    //   { last_login: new Date().toLocaleString() }
    // );
    //4.登录
    res.send({ errno: "0" });
  })();
});

router.post("/passport/logout", (req, res) => {
  //退出登录操作 将浏览器中的session清空
  delete req.session["user_id"];
  res.send({ errno: "0", errmsg: "退出登录成功" });
});

//与项目无关
router.get("/passport/jwt_token", (req, res) => {
  const token = jwt.sign({userName: '张三', age: 18}, keys.jwt_salt, {expiresIn: 60 * 60 * 2})
  res.send( {
    errmsg: 'success',
    reason: '登录请求'
  })
})



module.exports = router;
